import java.util.Scanner;

/**
 * Program to perform basic mathematical operations on two numbers
 * @author Steven Nguyen
 *
 */
public class JavadocPrac {

	/**
	 * Stores the first number to be entered into the equation
	 */
	public static  double num1;
	
	/**
	 */
	public static double num2;
	
	/**
	 * Scanner object in used to get user input
	 */
	public static Scanner in = new Scanner(System.in);

	/**
	 * This is the main function which will be called when the application runs.
	 * 
	 * @param args A string array containing the command line arguments
	 */
	public static void main(String[] args) {
		
		JavadocPrac jp = new JavadocPrac();
		System.out.print("Do you want to [a]dd, [s]ubtract, [m]ultiply or [d]ivide? ");
		char choice = in.next().charAt(0);

		switch(choice) {
		case 'a':
			System.out.println("You have selected add.");
			jp.prompt();
			System.out.println(num1 + " + " + num2 + " = " + jp.sum(num1, num2));
			break;
		case 's':
			System.out.println("You have selected subtract.");
			jp.prompt();
			System.out.println(num1 + " - " + num2 + " = " + jp.subtract(num1, num2));
			break;
		case 'm':
			System.out.println("You have selected multiply.");
			jp.prompt();
			System.out.println(num1 + " * " + num2 + " = " + jp.multiply(num1, num2));
			break;
		case 'd':
			System.out.println("You have selected divide.");
			jp.prompt();
			System.out.println(num1 + " / " + num2 + " = " + jp.divide(num1, num2));
			break;
		default:
			System.out.println("Invalid input, bye bye!");
		}

	}

	/**
	 * This method takes two numbers as parameters, then
	 * sums them together to return the result
	 * @param num1 The first number to be added 
	 * @param num2 The second number to be added
	 * @return
	 */
	public double sum(double num1, double num2) {
		return num1 + num2;
	}

	/**
	 * 
	 * @param num1
	 * @param num2
	 * @return
	 */
	public double subtract(double num1, double num2) {
		return num1 - num2;
	}

	/**
	 * 
	 * @param num1
	 * @param num2
	 * @return
	 */
	public double multiply(double num1, double num2) {
		return num1 * num2;
	}
	
	/**
	 * This method takes two numbers as parameters, then
	 * divides the first number by the second to return the
	 * result
	 * @param num1 The number being divided
	 * @param num2 The number num1 is divided by
	 * @return The result of dividing the first number by the second
	 */
	public double divide(double num1, double num2) {
		return num1 / num2;
	}

	/**
	 * 
	 */
	public void prompt() {
		System.out.print("Please enter the first number: ");
		num1 = in.nextDouble();
		System.out.print("Please enter the second number: ");
		num2 = in.nextDouble();
	}

}
